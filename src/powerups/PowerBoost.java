package powerups;

import bombermen.Bomberman;
import processing.ProcessingSketch;

public class PowerBoost extends Powerup {

	public PowerBoost(float x, float y, int cX, int cY) {
		super(x, y, cX, cY);
	}

	public void draw(ProcessingSketch sketch) {
		sketch.image(sketch.sprites.powerupSprites[sketch.sprites.POWER_SPRITE], x - (size / 2), y - (size / 2), size, size);
	}

	public void applyEffect(Bomberman b) {
		b.power = b.power + 1;
	}
}
