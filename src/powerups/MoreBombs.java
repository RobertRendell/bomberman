package powerups;

import bombermen.AIBomberman;
import bombermen.Bomberman;
import processing.ProcessingSketch;

public class MoreBombs extends Powerup {

	public MoreBombs(float x, float y, int cX, int cY) {
		super(x, y, cX, cY);
	}

	public void draw(ProcessingSketch sketch) {
		sketch.image(sketch.sprites.powerupSprites[sketch.sprites.BOMBS_SPRITE], x - (size / 2), y - (size / 2), size, size);
	}

	public void applyEffect(Bomberman b) {
		if (!(b instanceof AIBomberman)) {
			b.maxBombs = b.maxBombs + 1;
		}
	}
}
