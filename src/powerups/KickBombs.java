package powerups;

import bombermen.Bomberman;
import processing.ProcessingSketch;

public class KickBombs extends Powerup {

	public KickBombs(float x, float y, int cX, int cY) {
		super(x, y, cX, cY);
	}

	protected void fillColour() {
		
	}

	public void draw(ProcessingSketch sketch) {
		sketch.image(sketch.sprites.powerupSprites[sketch.sprites.KICK_SPRITE], x - (size / 2), y - (size / 2), size, size);
	}

	public void applyEffect(Bomberman b) {
		b.kickBombs = true;
	}
}
