package powerups;

import bombermen.Bomberman;
import processing.ProcessingSketch;

public class SpeedBoost extends Powerup {

	public SpeedBoost(float x, float y, int cX, int cY) {
		super(x, y, cX, cY);
	}

	public void draw(ProcessingSketch sketch) {
		sketch.image(sketch.sprites.powerupSprites[sketch.sprites.SPEED_SPRITE], x - (size / 2), y - (size / 2), size, size);
	}

	public void applyEffect(Bomberman b) {
		b.speed = b.speed + 1.0f;
	}
}