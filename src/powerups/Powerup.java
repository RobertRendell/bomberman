package powerups;

import bombermen.Bomberman;
import processing.ProcessingSketch;

public abstract class Powerup {

	public float x, y;
	public int cX, cY;
	public float size;
	public int powerupType;
	public boolean applied;

	public Powerup(float x, float y, int cX, int cY) {
		this.x = x;
		this.y = y;
		this.cX = cX;
		this.cY = cY;
		size = 35;
		applied = false;
	}

	public void draw(ProcessingSketch sketch) {
		fillColour();
		sketch.ellipse(x, y, size, size);
	}

	protected void fillColour(){
		
	}

	public void usePowerup(Bomberman b) {
		applyEffect(b);
		applied = true;
	}

	protected abstract void applyEffect(Bomberman b);
}