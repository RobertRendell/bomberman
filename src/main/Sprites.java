package main;

import main.Direction;
import processing.ProcessingSketch;
import processing.core.PImage;

public class Sprites{
	
	public final int KICK_SPRITE = 0;
	public final int BOMBS_SPRITE = 1;
	public final int POWER_SPRITE = 2;
	public final int SPEED_SPRITE = 3;
	
	public PImage[] bombermanSprites;
	public PImage[] powerupSprites;
	public PImage[] bombSprites;
	public PImage wallSprite;
	public PImage graveSprite;
	
	private ProcessingSketch processingSketch;
	
	public Sprites(ProcessingSketch processingSketch){
		this.processingSketch = processingSketch;
		loadSprites();
	}

	public void loadSprites(){
	    bombermanSprites = new PImage[4];
	    bombermanSprites[Direction.N.value] = loadImage("North.png");
	    bombermanSprites[Direction.E.value] = loadImage("East.png");
	    bombermanSprites[Direction.S.value] = loadImage("South.png");
	    bombermanSprites[Direction.W.value] = loadImage("West.png");
	    
	    powerupSprites = new PImage[4];
	    powerupSprites[KICK_SPRITE] = loadImage("Kick.png");
	    powerupSprites[BOMBS_SPRITE] = loadImage("Bombs.png");
	    powerupSprites[POWER_SPRITE] = loadImage("Power.png");
	    powerupSprites[SPEED_SPRITE] = loadImage("Speed.png");
	    
	    bombSprites = new PImage[2];
	    bombSprites[0] = loadImage("Bomb1.png");
	    bombSprites[1] = loadImage("Bomb2.png");
	    
	    wallSprite = loadImage("Wall.png");
	    graveSprite = loadImage("grave.png");
	}
	
	private PImage loadImage(String imagePath){
		return processingSketch.loadImage(imagePath);
	}
}
