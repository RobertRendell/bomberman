package main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Set;

import bombermen.BasicAIBomberman;
import bombermen.Bomberman;
import powerups.KickBombs;
import powerups.MoreBombs;
import powerups.PowerBoost;
import powerups.Powerup;
import powerups.SpeedBoost;
import processing.ProcessingSketch;
import processing.core.PVector;

public class BombermanGrid extends Grid {

	private int[][] field;
	private ArrayList<Powerup> powerups;
	private ArrayList<Bomberman> bombermen;
	private ArrayList<Bomb> bombs;
	
	private int numberOfHumanPlayers;
	private int numberOfAIPlayers=3;

	private boolean gameOver;
	public int gameResult;
	private int powerUpChance = 13;

	private Animated roundTimer;
	public int time;
	public int roundTimeLimit = 60;
	public boolean suddenDeath;
	private int suddenDeathRound = 0;
	private PVector suddenDeathBlock;

	private boolean damageEnabled = true;

	public final int OUT_OF_BOUNDS = -1,
					 EMPTY = 0,
					 UNBREAKABLE = 1,
				     BREAKABLE = 2,
				     EXPLOSION = 3,
				     POWERUP = 4;

	public final int NONE = -1,
					 LOSE = 0,
					 WIN = 1;
	
	public BombermanGrid(float x, float y, float w, float h, int cellsX, int cellsY, int numberOfHumanPlayers) {
		super(x, y, w, h, cellsX, cellsY);
		field = new int[cellsX][cellsY];
		powerups = new ArrayList<Powerup>();
		bombermen = new ArrayList<Bomberman>();
		bombs = new ArrayList<Bomb>();
		generateField();
		this.numberOfHumanPlayers = numberOfHumanPlayers;
		addPlayers(numberOfHumanPlayers);
		time = roundTimeLimit;
		roundTimer = new Animated(1000);
		suddenDeath = false;
		suddenDeathRound = 0;
		suddenDeathBlock = new PVector(0, 0);
		gameOver = false;
		gameResult = -1;
	}

	private void addPlayers(int numberOfHumanPlayers) {
		int[] xSpawn = new int[4];
		int[] ySpawn = new int[4];
		Direction[] facing = new Direction[4];

		xSpawn[0] = 0;
		ySpawn[0] = 0;
		facing[0] = Direction.E;
		xSpawn[1] = sizeW - 1;
		ySpawn[1] = 0;
		facing[1] = Direction.S;
		xSpawn[2] = sizeW - 1;
		ySpawn[2] = sizeH - 1;
		facing[2] = Direction.W;
		xSpawn[3] = 0;
		ySpawn[3] = sizeH - 1;
		facing[3] = Direction.N;
		
		bombermen.add( new Bomberman(0, 0, Direction.E, 1) );
		if(numberOfHumanPlayers == 2){
			bombermen.add(new Bomberman(0, 0, Direction.E, 2));
		}else{
			bombermen.add(new BasicAIBomberman(0, 0, Direction.S, 2, this));
		}
		if(numberOfAIPlayers > 1){
			bombermen.add(new BasicAIBomberman(0, 0, Direction.N, 3, this));
			bombermen.add(new BasicAIBomberman(0, 0, Direction.N, 4, this));
		}

		Collections.shuffle(bombermen);
		for (int i = 0; i < bombermen.size(); i++) {
			Bomberman man = bombermen.get(i);
			man.setLocation(cellXToX(xSpawn[i]), cellYToY(ySpawn[i]));
			man.facing = facing[i];
		}
	}

	public void draw(ProcessingSketch sketch) {
		super.draw(sketch);
		drawBombs(sketch);
		drawBombermen(sketch);
		drawPowerups(sketch);
		if (hasTicked()) {
			putOutFires();
			if (suddenDeath) {
				dropSuddenDeathBlock();
			}
		}
		checkRoundTimer();
		
	}
	
	private void drawBombs(ProcessingSketch sketch){
		for (int i = bombs.size() - 1; i >= 0; i--) {
			Bomb b = bombs.get(i);
			if (!b.exploded) {
				b.draw(sketch);
			} else {
				explode(b);
			}
		}
	}
	
	private void drawBombermen(ProcessingSketch sketch){
		for (Bomberman man : bombermen) {
			if (!man.dead) {
				snapBombermanOntoTracks(man);
				man.draw(sketch);
			}else{
				sketch.image(sketch.sprites.graveSprite, man.centre().x, man.centre().y,cellW,cellH);
			}
		}
	}
	
	private void snapBombermanOntoTracks(Bomberman man){
		PVector centre = man.centre();
		if( man.facing == Direction.N || man.facing == Direction.S ){
			float snapX = cellXToX( xToCellX( centre.x ) );
			man.setLocation( snapX, centre.y);
		}else if( man.facing == Direction.E || man.facing == Direction.W ){
			float snapY = cellYToY( yToCellY( centre.y ) );
			man.setLocation( centre.x, snapY);
		}
	}
	
	private void drawPowerups(ProcessingSketch sketch){
		for (int i = powerups.size() - 1; i >= 0; i--) {
			Powerup p = powerups.get(i);
			if (!p.applied) {
				p.draw(sketch);
			} else {
				powerups.remove(p);
			}
		}
	}

	private boolean isInSuddenDeathArea(int cX, int cY) {
		return suddenDeath && (cX == suddenDeathBlock.x || cY == suddenDeathBlock.y);
	}

	private void checkRoundTimer() {
		if (roundTimer.hasTicked()) {
			time = time - 1;
			if (time <= 0) {
				suddenDeath = true;
				time = 150;
			}
		}
	}

	private void dropSuddenDeathBlock() {
		int sdX = (int) suddenDeathBlock.x;
		int sdY = (int) suddenDeathBlock.y;
		field[sdX][sdY] = UNBREAKABLE;
		killBombermenAtCell(sdX, sdY);
		determineNextSuddenDeathBlock(sdX, sdY);
	}

	private void determineNextSuddenDeathBlock(int x, int y) {
		int sdr = suddenDeathRound;

		if (x == sdr && y == sdr) {
			// starting point
			x = x + 1;
		} else if (y == sdr && x < (sizeW - 1 - sdr)) {
			x = x + 1;
		} else if (x == (sizeW - 1 - sdr) && y < (sizeH - 1 - sdr)) {
			y = y + 1;
		} else if (y == (sizeH - 1 - sdr) && x > sdr) {
			x = x - 1;
		} else if (x == sdr && y <= (sizeH - 1 - sdr)) {
			if (y == sdr + 1) {
				suddenDeathRound = suddenDeathRound + 1;
				x = suddenDeathRound;
				y = suddenDeathRound;
			} else {
				y = y - 1;
			}
		}
		suddenDeathBlock = new PVector(x, y);
	}
	
	public boolean isValidMove(Bomberman man) {
		PVector c = man.centre();
		int cX = xToCellX(c.x);
		int cY = yToCellY(c.y);

		if (withinGameBounds(c.x, c.y) && withinBounds(cX, cY)) {
			int f = field[cX][cY];
			if (f != UNBREAKABLE && f != BREAKABLE) {
				Bomb b = cellContainsBomb(cX, cY);
				if (b != null && !b.owner.equals(man)) {
					return false;
				}
				checkForPowerup(cX, cY, man);
				// if you run into an explosion, you should die. ai bombermen kill themselves with this
				// at the moment, you only die if you are in the cell when the bomb goes off
				// checkForExplosion(cX,cY,man);
				return true;
			}
		}
		return false;
	}

	private boolean withinGameBounds(float x, float y) {
		return x >= 0 && x < w && y >= 0 && y < h;
	}

	private boolean withinBounds(int cellX, int cellY) {
		return cellX >= 0 && cellX < sizeW && cellY >= 0 && cellY < sizeH;
	}

	private Bomb cellContainsBomb(int x, int y) {
		for (Bomb b : bombs) {
			if (xToCellX(b.x) == x && yToCellY(b.y) == y) {
				return b;
			}
		}
		return null;
	}

	private void checkForPowerup(int cX, int cY, Bomberman man) {
		for (Powerup p : powerups) {
			if (p.cX == cX && p.cY == cY) {
				p.usePowerup(man);
			}
		}
	}

	// see isValidMove why this isn't used yet.
	private void checkForExplosion(int cX, int cY, Bomberman man) {
		if (field[cX][cY] == EXPLOSION) {
			killBomberman(man);
		}
	}

	private void killBomberman(Bomberman man) {
		if (damageEnabled) {
			man.kill();
			if (man.playerNo == 1 && numberOfHumanPlayers == 1) {
				gameOver(LOSE);
			}

			if (noOfBombermenAlive() == 1) {
				gameOver(NONE);
			}
		}
	}

	private int noOfBombermenAlive() {
		int aliveBombermen = 0;
		for (Bomberman bm : bombermen) {
			if (!bm.dead) {
				aliveBombermen = aliveBombermen + 1;
			}
		}
		return aliveBombermen;
	}

	private void gameOver(int result) {
		gameOver = true;
		gameResult = result;
	}

	public int getFieldValFromCoords(float x, float y) {
		int cX = xToCellX(x);
		int cY = yToCellY(y);
		return getFieldVal(cX, cY);
	}

	public int getFieldVal(int cX, int cY) {
		if (withinBounds(cX, cY)) {
			return field[cX][cY];
		}
		return OUT_OF_BOUNDS;
	}

	public int xToCellX(float x) {
		return (int) (x / cellW);
	}

	public int yToCellY(float y) {
		return (int) (y / cellH);
	}

	public float cellXToX(int x) {
		return (x * cellW) + (cellW / 2);
	}

	public float cellYToY(int y) {
		return (y * cellH) + (cellH / 2);
	}

	private void generateField() {
		for (int row = 0; row < sizeH; row++) {
			for (int col = 0; col < sizeW; col++) {
				if (col % 2 == 0 && row % 2 == 0) {
					field[col][row] = UNBREAKABLE;
				} else {
					Random r = new Random();
					int emptyChance = r.nextInt(6);
					if ((int) emptyChance == 1) {
						field[col][row] = EMPTY;
					} else {
						field[col][row] = BREAKABLE;
					}
				}
			}
		}
		// Starting zones for players
		field[0][0] = EMPTY;
		field[0][1] = EMPTY;
		field[1][0] = EMPTY;
		field[sizeW - 1][0] = EMPTY;
		field[sizeW - 1][1] = EMPTY;
		field[sizeW - 2][0] = EMPTY;
		field[sizeW - 1][sizeH - 1] = EMPTY;
		field[sizeW - 1][sizeH - 2] = EMPTY;
		field[sizeW - 2][sizeH - 1] = EMPTY;
		field[0][sizeH - 1] = EMPTY;
		field[0][sizeH - 2] = EMPTY;
		field[1][sizeH - 1] = EMPTY;
	}

	private void putOutFires() {
		for (int row = 0; row < sizeH; row++) {
			for (int col = 0; col < sizeW; col++) {
				if (field[col][row] == EXPLOSION) {
					field[col][row] = EMPTY;
				}
			}
		}
	}

	protected void drawCell(int x, int y, ProcessingSketch sketch) {
		sketch.stroke(0);
		sketch.noStroke();
		switch (field[x][y]) {
		case UNBREAKABLE:
			sketch.fill(100, 100, 100);
			sketch.stroke(0);
			sketch.strokeWeight(3);
			basicCell(x, y, sketch);
			break;
		case BREAKABLE:
			sketch.image(sketch.sprites.wallSprite, x * cellW, y * cellH, cellW, cellH);
			// fill(200,200,200);
			// stroke(0);
			// strokeWeight(1);
			// basicCell(x,y);
			break;
		case EXPLOSION:
			sketch.fill(255, 0, 0);
			basicCell(x, y, sketch);
			break;
		default:
			sketch.fill(0, 255, 0);
			basicCell(x, y, sketch);
			break;
		}

	}

	private void basicCell(int x, int y, ProcessingSketch sketch) {
		sketch.rect(x * cellW, y * cellH, cellW, cellH);
	}

	public Bomb plantBomb(Bomberman man) {
		Bomb bomb = null;
		if (man.canPlantBomb()) {
			int cX = xToCellX(man.centre().x);
			int cY = yToCellY(man.centre().y);
			if (cellContainsBomb(cX, cY) == null) {
				float bX = cellXToX(cX);
				float bY = cellYToY(cY);
				bomb = new Bomb(bX, bY, man);
				bombs.add(bomb);
			}
		}
		return bomb;
	}

	public void plantPlayerBomb(int playerNo) {
		plantBomb(getBombermanByPlayerNo(playerNo));
	}
	
	public Bomberman getBombermanByPlayerNo(int playerNo){
		for(Bomberman man : bombermen){
			if(man.playerNo == playerNo){
				return man;
			}
		}
		return null;
	}

	private void explode(Bomb b) {
		// loop through until you hit a wall or breakable wall or reach the
		// distance
		int cx = xToCellX(b.x);
		int cy = yToCellY(b.y);
		bombs.remove(b);
		for(Direction direction : Direction.values()){
			explodeCell(cx, cy, 0, b, direction);
		}
	}
	
	public PVector neighbourCoordinate(int originCellX, int originCellY, Direction dir, int distance){
		int x = originCellX;
		int y = originCellY;
		switch (dir) {
			case N: y = y - distance; break;
			case E: x = x + distance; break;
			case S: y = y + distance; break;
			case W: x = x - distance; break;
		}
		return new PVector(x,y);
	}
	
	public List<PVector> neighbouringCoordinates(int cX, int cY) {
		return neighbouringCoordinates(cX,cY,false);
	}
	
	public List<PVector> neighbouringCoordinates(int cX, int cY, boolean onlyValidPathNeighbours){
		List<PVector> neighbours = new ArrayList<PVector>();
		for(Direction direction : Direction.values()){
			PVector neighbour = neighbourCoordinate(cX,cY,direction,1);
			if(onlyValidPathNeighbours){
				if(isCellFree((int)neighbour.x,(int)neighbour.y)){
					neighbours.add(neighbour);
				}
			}else{
				neighbours.add(neighbour);
			}
		}
		return neighbours;
	}
	
	private boolean isCellFree(int cX, int cY){
		if(withinBounds(cX,cY)){
			int cV = field[cX][cY];
			return (cV != UNBREAKABLE && cV != BREAKABLE && (cellContainsBomb(cX,cY) == null));
		}
		return false;
	}

	private void explodeCell(int originCellX, int originCellY, int currentBlastRadius, Bomb bomb, Direction dir) {
		// blast radius increases with every recursive call, use x and y to find the next cells affected.
		PVector neighbourCoordinate = neighbourCoordinate(originCellX,originCellY,dir,currentBlastRadius);
		int x = (int)neighbourCoordinate.x;
		int y = (int)neighbourCoordinate.y;

		if (withinBounds(x, y) && currentBlastRadius <= bomb.owner.power) {
			if (field[x][y] != UNBREAKABLE) {
				int cellContentsBeforeExplosion = field[x][y];
				field[x][y] = EXPLOSION;
				
				if (cellContentsBeforeExplosion == BREAKABLE) {
					chanceOfPowerup(x, y);
				} else {
					explodeCell(originCellX, originCellY, currentBlastRadius + 1, bomb, dir);
				}
				// Blow up other bombs caught in the explosion.
				Bomb b = cellContainsBomb(x,y);
				if( b != null ){
					explode(b);
				}

				killBombermenAtCell(x, y);
			}
		}
	}

	private void killBombermenAtCell(int x, int y) {
		for (Bomberman man : bombermen) {
			if (xToCellX(man.centre().x) == x && yToCellY(man.centre().y) == y) {
				killBomberman(man);
			}
		}
	}

	private void chanceOfPowerup(int x, int y) {
		Random rnd = new Random();
		int r = rnd.nextInt(powerUpChance);

		if (r == 0) {
			field[x][y] = POWERUP;
			generatePowerup(x, y);
		}
	}

	private void generatePowerup(int cX, int cY) {
		Powerup p;
		float x = cellXToX(cX);
		float y = cellYToY(cY);
		Random r = new Random();
		
		int powerupType = r.nextInt(4);

		switch (powerupType) {
		case 0:
			p = new SpeedBoost(x, y, cX, cY);
			break;
		case 1:
			p = new KickBombs(x, y, cX, cY);
			break;
		case 2:
			p = new PowerBoost(x, y, cX, cY);
			break;
		default:
			p = new MoreBombs(x, y, cX, cY);
			break;
		}
		if (p != null) {
			powerups.add(p);
		}
	}

	public void moveLeft(int playerNo) {
		Bomberman player = getBombermanByPlayerNo(playerNo);
		player.moveLeft();
		if (!isValidMove(player)) {
			player.moveRight();
		}
	}

	public void moveRight(int playerNo) {
		Bomberman player = getBombermanByPlayerNo(playerNo);
		player.moveRight();
		if (!isValidMove(player)) {
			player.moveLeft();
		}
	}

	public void moveUp(int playerNo) {
		Bomberman player = getBombermanByPlayerNo(playerNo);
		player.moveUp();
		if (!isValidMove(player)) {
			player.moveDown();
		}
	}

	public void moveDown(int playerNo) {
		Bomberman player = getBombermanByPlayerNo(playerNo);
		player.moveDown();
		if (!isValidMove(player)) {
			player.moveUp();
		}
	}
	
	public boolean isGameOver(){
		return gameOver;
	}
	
	public boolean isSuddenDeath(){
		return suddenDeath;
	}

}
