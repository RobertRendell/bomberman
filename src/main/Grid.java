package main;

import processing.ProcessingSketch;
import processing.core.PVector;

public class Grid extends Animated {

	public PVector location;
	public int sizeW, sizeH;
	public float cellW, cellH, w, h;
	public boolean gridLines;

	public Grid(float x, float y, float w, float h, int cellsX, int cellsY) {
		super(500);
		location = new PVector(x, y);
		sizeW = cellsX;
		sizeH = cellsY;
		cellW = w / sizeW;
		cellH = h / sizeH;
		this.w = w;
		this.h = h;
		gridLines = false;
	}

	public void draw(ProcessingSketch sketch) {
		drawBackground(sketch);
		for (int y = 0; y < sizeH; y++) {
			drawRow(y,sketch);
			if (gridLines) {
				sketch.stroke(0);
				sketch.fill(0);
				sketch.line(location.x, location.y + (cellH * y), location.x + w, location.y + (cellH * y));
			}
		}
	}

	protected void drawRow(int y, ProcessingSketch sketch) {
		for (int x = 0; x < sizeW; x++) {
			drawCell(x, y,sketch);
			if (gridLines) {
				sketch.stroke(0);
				sketch.fill(0);
				sketch.line(location.x + (cellW * x), location.y, location.x + (cellW * x), location.y + (h));
			}
		}
	}

	protected void drawCell(int x, int y, ProcessingSketch sketch) {
		sketch.rect(x, y, cellW, cellH);
	}

	protected void drawBackground(ProcessingSketch sketch) {
		sketch.fill(255);
		sketch.noStroke();
		sketch.rect(location.x, location.y, w, h);
	}
}
