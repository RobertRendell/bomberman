package main;

import java.util.ArrayList;

import bombermen.Bomberman;
import processing.ProcessingSketch;
import processing.core.PVector;

public class Bomb extends Animated {

	public float x, y;
	public int colour;
	public int fuse;
	public float size;
	public boolean exploded;
	public Bomberman owner;
	public ArrayList<PVector> explosion;
	private float sizeVariant = 5.0f;

	public Bomb(float bX, float bY, Bomberman owner) {
		super(50);
		this.x = bX;
		this.y = bY;
		size = 35.0f;
		fuse = 2000 / interval;
		exploded = false;
		this.owner = owner;
		lightFuse();
	}

	public void lightFuse() {
		owner.bombPlanted();
	}

	public void draw(ProcessingSketch sketch) {
		float size;
		if (!exploded) {
			if (colour == 0) {
				size = this.size - sizeVariant;
			} else {
				size = this.size + sizeVariant;
			}
			if (hasTicked()) {
				colour = colour + 1;
				if (colour > 1) {
					colour = 0;
				}
				fuse = fuse - 1;
				if (fuse <= 0) {
					explode();
				}
			}
			sketch.image(sketch.sprites.bombSprites[colour], x - (size / 2), y - (size / 2), size, size);
			// ellipse(x, y, size, size);
		}
	}

	public int power() {
		return owner.power;
	}

	public void explode() {
		exploded = true;
		owner.bombExploded();
	}
}
