package main;

import java.util.Random;


public enum Direction{
	N(0),E(1),S(2),W(3);
	public int value;
	Direction(int value){
		this.value = value;
	}
	public static Direction direction(int d){
		for(Direction dir : Direction.values()){
			if(dir.value == d){
				return dir;
			}
		}
		return null;
	}
	public static Direction opposite(Direction d){
		switch(d){
		case N: return S;
		case E: return W;
		case S: return N;
		case W: return E;
		default: return null;
		}
	}
	public static Direction randomDirection(){
		Random r = new Random();
		return direction(r.nextInt(Direction.values().length));
	}
}