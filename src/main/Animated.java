package main;

public class Animated {

	public int interval;
	private long lastTime = 0;
	protected boolean enabled;

	public Animated(int interval) {
		this.interval = interval;
		initialise();
	}

	public Animated() {
		initialise();
	}

	private void initialise() {
		lastTime = System.currentTimeMillis();
		enabled = true;
	}

	public boolean hasTicked() {
		if (System.currentTimeMillis() - lastTime > interval) {
			lastTime = System.currentTimeMillis();
			return true;
		} else {
			return false;
		}
	}
}
