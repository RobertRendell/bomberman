package bombermen;

import java.util.List;
import java.util.Random;

import main.Bomb;
import main.BombermanGrid;
import main.Direction;
import processing.core.PVector;

public class BasicAIBomberman extends AIBomberman {

	// Basic AI should:
	// - use only one bomb
	// - be able to plant a bomb next to a breakable wall
	// then hide out of the line of fire
	// - repeat

	private Bomb bomb;
	private int bX, bY;
	private int state;

	private final int PLANTING = 1;
	private final int RUNNING_AWAY = 2;
	private final int HIDING = 3;

	public BasicAIBomberman(float x, float y, Direction facing, int playerNo, BombermanGrid grid) {
		super(x, y, facing, playerNo, grid);
		state = PLANTING;
	}

	protected boolean isSafeFromOwnBomb(int cX, int cY) {
		return ((facing == Direction.N || facing == Direction.S) && (Math.abs(cY - bY) > bomb.power()))
				|| ((facing == Direction.E || facing == Direction.W) && (Math.abs(cX - bX) > bomb.power()))
				|| (cX != bX && cY != bY);
	}

	protected void moveAI() {
		float pX = x;
		float pY = y;
		int cX = grid.xToCellX(centre().x);
		int cY = grid.yToCellY(centre().y);
		if (state == RUNNING_AWAY) {
			if (isSafeFromOwnBomb(cX, cY)) {
				// println("bomb("+bX+","+bY+") hide("+cX+","+cY+")");
				state = HIDING;
			}
			if (bombsPlanted == 0) {
				state = PLANTING;
			}
		}
		if (state != HIDING) {
			moveInFacingDir();
			cX = grid.xToCellX(centre().x);
			cY = grid.yToCellY(centre().y);
			if (!grid.isValidMove(this)) {
				int potentialCell = grid.getFieldVal(cX, cY);
				x = pX;
				y = pY;
				cX = grid.xToCellX(centre().x);
				cY = grid.yToCellY(centre().y);
				if (state == PLANTING) {
					if (potentialCell == grid.BREAKABLE) {
						plantBomb(cX, cY);
						state = RUNNING_AWAY;
						chooseEscapeDirection();
					} else {
						facing = Direction.randomDirection();
					}
				} else {
					facing = Direction.randomDirection();
				}
			} else {
				if (state == PLANTING) {
					Random r = new Random();
					if (r.nextInt(10) == 1) {
						facing = Direction.randomDirection();
					}
				}
			}
		}
		if (state == HIDING) {
			if (bombsPlanted == 0) {
				state = PLANTING;
			}
		}
		// println("state:"+state+"--"+this);
	}
	
	protected void chooseEscapeDirection(){
		List<PVector> validPathNeighbours = grid.neighbouringCoordinates(cellX(), cellY(), true);
		if(validPathNeighbours.size() > 0){
			Random r = new Random();
			PVector pickedNeighbour;
			pickedNeighbour = validPathNeighbours.get(r.nextInt(validPathNeighbours.size()));
			faceNeighbour((int)pickedNeighbour.x,(int)pickedNeighbour.y);
		}else{
			facing = Direction.randomDirection();
		}
	}
	
	protected void faceNeighbour(int neighbourCX, int neighbourCY){
		if(cellX() < neighbourCX){ facing = Direction.W; }
		else if(cellX() > neighbourCX){ facing = Direction.E; }
		else if(cellY() < neighbourCY){ facing = Direction.S; }
		else if(cellY() > neighbourCY){ facing = Direction.N; }
	}

	protected int cellX() {
		return grid.xToCellX(centre().x);
	}

	protected int cellY() {
		return grid.yToCellY(centre().y);
	}

	protected void movedCell() {

	}

	protected void plantBomb(int cX, int cY) {
		bomb = grid.plantBomb(this);
		bX = cX;
		bY = cY;
	}

}
