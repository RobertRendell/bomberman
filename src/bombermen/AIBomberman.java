package bombermen;

import java.util.Random;

import main.BombermanGrid;
import main.Direction;
import processing.ProcessingSketch;

public abstract class AIBomberman extends Bomberman {

	protected BombermanGrid grid;

	public AIBomberman(float x, float y, Direction facing, int playerNo, BombermanGrid grid) {
		super(x, y, facing, playerNo);
		this.grid = grid;
	}

	public void draw(ProcessingSketch sketch) {
		super.draw(sketch);
		moveAI();
	}

	protected abstract void moveAI();
	
	protected void moveInFacingDir() {
		switch (facing) {
		case N:
			moveUp();
			break;
		case E:
			moveRight();
			break;
		case S:
			moveDown();
			break;
		case W:
			moveLeft();
			break;
		}
	}
}
