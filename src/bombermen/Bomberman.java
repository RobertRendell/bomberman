package bombermen;

import main.Direction;
import processing.ProcessingSketch;
import processing.core.PImage;
import processing.core.PVector;

public class Bomberman{
  
  public int playerNo;
  protected float x,y;
  public Direction facing;
  public float speed;
  public float size;
  public int bombsPlanted;
  public int maxBombs;
  public int power;
  public boolean dead;
  public boolean kickBombs;
  PImage[] sprites;
  
  public Bomberman(float x, float y, Direction facing, int playerNo){
    this.x = x;
    this.y = y;
    this.playerNo = playerNo;
    speed = 3.0f;
    size = 40;
    bombsPlanted = 0;
    maxBombs = 1;
    power = 1;
    this.facing = facing;
  }
  
  public void draw(ProcessingSketch processingSketch){
    processingSketch.image(processingSketch.sprites.bombermanSprites[facing.value], x, y, size, size);
    processingSketch.fill(0);
    processingSketch.text("P"+playerNo,x-10,y-10);
  }
  
  public PVector centre(){
    return new PVector(x+(size/2),y+(size/2));
  }
  
  public void kill(){
    dead = true;
  }
  
  public float top(){
    return 0;
  }
  public float bottom(){
    return 0;
  }
  public float left(){
    return 0;
  }
  public float right(){
    return 0;
  }
  
  public void moveLeft(){
    facing = Direction.W;
    x = x - speed;
  }
  
  public void moveRight(){
    facing = Direction.E;
    x = x + speed;
  }
  
  public void moveDown(){
    facing = Direction.S;
    y = y + speed;
  }
  
  public void moveUp(){
    facing = Direction.N;
    y = y - speed;
  }
  
  public boolean canPlantBomb(){
    return bombsPlanted < maxBombs;
  }
  
  public void bombPlanted(){
    bombsPlanted = bombsPlanted + 1;
  }
  
  public void bombExploded(){
    bombsPlanted = bombsPlanted - 1;
  }
  
  public void setLocation(float x, float y){
	  // stupidly x and y store the top left coordinates and centre() adds the offset
	  // it should really be the other way round.
	  this.x = x-(size/2);
	  this.y = y-(size/2);
  }
  
  public float getX(){
	  return x;
  }
  
  public float getY(){
	  return y;
  }
  
  //public boolean equals
  
}
