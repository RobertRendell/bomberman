package processing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import main.BombermanGrid;
import main.Sprites;
import processing.ProcessingSketch.GameControl;
import processing.core.PApplet;
import ui.UserInterface;

public class ProcessingSketch extends PApplet {

	private Set<Character> keysPressed;
	private BombermanGrid bombermanGrid;
	private int width = 400, height = 400;
	private UserInterface ui;
	private int numberOfHumanPlayers = 1;

	public Sprites sprites;

	// The index of the list indicates the player number
	private List<Map<GameControl, Character>> playerKeyboardMappings;

	public void settings() {
		size(width, height);
	}

	public void setup() {
		float uiHeight = 40.0f;
		bombermanGrid = new BombermanGrid(0, 0, width, height - uiHeight, 13, 11, numberOfHumanPlayers);
		keysPressed = new HashSet<Character>();
		ui = new UserInterface(bombermanGrid, width, height, uiHeight);
		sprites = new Sprites(this);
		setupKeyboardMappings();
		ellipseMode(CENTER);
	}
	
	public void draw() {
		controlHumanPlayers();
		if (!bombermanGrid.isGameOver()) {
			bombermanGrid.draw(this);
		} else {
			delay(2000);
			setup();
		}
		ui.draw(this);
	}
	
	private void setupKeyboardMappings(){
		playerKeyboardMappings = new ArrayList<Map<GameControl, Character>>();
		// Player 1
		playerKeyboardMappings.add(createKeyboardMapping(' ','s','a','d','w'));
		// Player 2
		playerKeyboardMappings.add(createKeyboardMapping(ENTER,'5','4','6','8'));
	}
	
	private void controlHumanPlayers() {
		if (keysDown()) {
			if (numberOfHumanPlayers > 0) {
				controlPlayer(1);
				if (numberOfHumanPlayers > 1) {
					controlPlayer(2);
				}
			}
		}
	}

	private void controlPlayer(int playerNo) {
		Map<GameControl, Character> keyboardMapping = playerKeyboardMappings.get(playerNo - 1);
		if (!bombermanGrid.getBombermanByPlayerNo(playerNo).dead) {
			if (keysPressed.contains(keyboardMapping.get(GameControl.PLANT_BOMB))) {
				bombermanGrid.plantPlayerBomb(playerNo);
			} else if (keysPressed.contains(keyboardMapping.get(GameControl.DOWN))) {
				bombermanGrid.moveDown(playerNo);
			} else if (keysPressed.contains(keyboardMapping.get(GameControl.UP))) {
				bombermanGrid.moveUp(playerNo);
			} else if (keysPressed.contains(keyboardMapping.get(GameControl.LEFT))) {
				bombermanGrid.moveLeft(playerNo);
			} else if (keysPressed.contains(keyboardMapping.get(GameControl.RIGHT))) {
				bombermanGrid.moveRight(playerNo);
			}
		}
	}
	
	private Map<GameControl, Character> createKeyboardMapping(Character plantBomb,Character down,Character left,Character right,Character up){
		Map<GameControl, Character> mappings = new HashMap<GameControl, Character>();
		mappings.put(GameControl.PLANT_BOMB,plantBomb);
		mappings.put(GameControl.DOWN,down);
		mappings.put(GameControl.LEFT,left);
		mappings.put(GameControl.RIGHT,right);
		mappings.put(GameControl.UP,up);
		return mappings;
	}

	public enum GameControl {
		PLANT_BOMB(0), UP(1), DOWN(2), LEFT(3), RIGHT(4);
		private int value;

		GameControl(int value) {
			this.value = value;
		}
	}

	public static void main(String[] args) {
		PApplet.main("processing.ProcessingSketch");
	}

	public void keyReleased() {
		keysPressed.remove(key);
	}

	private boolean keysDown() {
		return keysPressed.size() > 0;
	}

	public void keyPressed() {
		keysPressed.add(key);
	}

}
