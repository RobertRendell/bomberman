package ui;

import main.BombermanGrid;
import processing.ProcessingSketch;

public class UserInterface {
	public float UIHeight;
	private BombermanGrid bombermanGrid;
	private float width, height;

	public UserInterface(BombermanGrid bombermanGrid, float width, float height, float uiHeight) {
		this.bombermanGrid = bombermanGrid;
		this.width = width;
		this.height = height;
		this.UIHeight = uiHeight;
	}

	public void draw(ProcessingSketch sketch) {
		sketch.stroke(0);
		sketch.strokeWeight(5);
		sketch.fill(255);
		sketch.rect(0, height - UIHeight, width, UIHeight);
		drawTimer(sketch);
		drawGameResult(sketch);
	}

	private void drawTimer(ProcessingSketch sketch) {
		if (bombermanGrid.isSuddenDeath()) {
			sketch.fill(255, 0, 0);
		} else {
			sketch.fill(0);
		}
		sketch.textSize(32);
		sketch.text(bombermanGrid.time, width / 2, height - UIHeight + 32);
	}

	private void drawGameResult(ProcessingSketch sketch) {
		if (bombermanGrid.isGameOver()) {
			if (bombermanGrid.gameResult == bombermanGrid.LOSE) {
				sketch.fill(255, 0, 0);
				sketch.text("YOU LOSE", width / 2, height - UIHeight + 150);
			} else {
				sketch.fill(0, 255, 0);
				sketch.text("YOU WIN", width / 2, height - UIHeight + 150);
			}
		}
	}
}
